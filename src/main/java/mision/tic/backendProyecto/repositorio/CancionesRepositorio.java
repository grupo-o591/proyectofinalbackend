package mision.tic.backendProyecto.repositorio;

import mision.tic.backendProyecto.modelo.Canciones;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CancionesRepositorio extends JpaRepository<Canciones,Integer> {
}
