package mision.tic.backendProyecto.controlador;

import mision.tic.backendProyecto.entradas.UsuarioEntrada;
import mision.tic.backendProyecto.modelo.Usuario;
import mision.tic.backendProyecto.servicio.UsuarioServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class UsuarioControlador {
    @Autowired
    UsuarioServicio usuarioServicio;

    @GetMapping("/iniciarsesion")
    public boolean iniciarsesion(@RequestParam String correo, @RequestParam String contraseña){
        return usuarioServicio.iniciarsesion(correo, contraseña);
    }

    @GetMapping("/consultarusuario")
    public Usuario consultarusuario(@RequestParam int id){
        return usuarioServicio.consultarusuario(id);
    }

    @PostMapping("/agregarusuario")
    public boolean agregarusuario(@RequestBody UsuarioEntrada usuarioEntrada){
        System.out.println(usuarioEntrada.getContraseña().isEmpty());
        return usuarioServicio.agregarusuario(usuarioEntrada);
    }

    @DeleteMapping("/eliminarusuario")
    public boolean eliminarusuario(@RequestParam int id){
        return usuarioServicio.eliminarusuario(id);
    }
}
