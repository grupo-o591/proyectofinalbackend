package mision.tic.backendProyecto.controlador;


import mision.tic.backendProyecto.modelo.Generos;
import mision.tic.backendProyecto.servicio.GeneroServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class GeneroControlador {
    @Autowired
    GeneroServicio generoServicio;

    @GetMapping("/obtenerGeneros")
    public List<Generos> obtenerGeneros(){
        return generoServicio.obtenerGeneros();
    }

    @GetMapping("/obtenerGeneroId")
    public Optional<Generos> obtenerGeneroId(@RequestParam int id){
        return generoServicio.obtenerGeneroId(id);
    }

    @PostMapping("/crearGenero")
    public boolean crearGenero(@RequestBody Generos generos){
        return generoServicio.crearGenero(generos);
    }
}
